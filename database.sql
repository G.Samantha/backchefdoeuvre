-- Active: 1673947756043@@127.0.0.1@3306@back_chefdoeuvre

DROP TABLE IF EXISTS animal_circuit;

DROP TABLE IF EXISTS reservation;

DROP TABLE IF EXISTS user;

DROP TABLE IF EXISTS circuit;

DROP TABLE IF EXISTS animal;


CREATE TABLE
    user (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR (255) NOT NULL,
        lastname VARCHAR (255),
        phone VARCHAR (255),
        email VARCHAR (255) UNIQUE NOT NULL,
        password VARCHAR (255) NOT NULL,
        role VARCHAR (255) NOT NULL
    );

CREATE TABLE
    animal (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR (255) NOT NULL,
        img VARCHAR (255),
        description VARCHAR (1000)
    );

CREATE TABLE
    circuit (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR (255) NOT NULL,
        img VARCHAR (255),
        description VARCHAR (1000)
    );

CREATE TABLE
    reservation (
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        date DATE NOT NULL,
        id_animal INT,
        FOREIGN KEY (id_animal) REFERENCES animal(id)
        ON DELETE CASCADE,
        id_circuit INT,
        FOREIGN KEY (id_circuit) REFERENCES circuit(id)
        ON DELETE CASCADE,
        id_user INT,
        FOREIGN KEY (id_user) REFERENCES user(id)
        ON DELETE CASCADE
    );

CREATE TABLE
    animal_circuit (
        id_animal INT,
        id_circuit INT,
        PRIMARY KEY (id_animal, id_circuit),
        Foreign Key (id_animal) REFERENCES animal(id) ON DELETE CASCADE,
        Foreign Key (id_circuit) REFERENCES circuit(id) ON DELETE CASCADE
    );

INSERT INTO
    animal (name, img, description)
VALUES (
        'Alpaga',
        'https://images.pexels.com/photos/3396661/pexels-photo-3396661.jpeg',
        'L\'alpaga est un herbivore de la famille des chameaux (camélidés) originaire d\'Amérique du Sud. Il serait une version domestiquée de la vigogne, qui vit sur les hauts plateaux des Andes. Sa laine fournie et d\'excellente qualité a fait sa renommée dans l\'industrie textile'
    ), (
        'Lama',
        'https://images.pexels.com/photos/3727251/pexels-photo-3727251.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Mammifère ruminant appartenant à la famille des Camélidés, d\assez grande taille, à long cou et hautes pattes, vivant dans les Andes à des altitudes élevées, et dont la toison laineuse sert à fabriquer des tissus très appréciés. L\'alpaga et la vigogne sont des variétés de lamas.'
    )
    ,(
        'Patou',
        'https://images.pexels.com/photos/7440123/pexels-photo-7440123.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Le chien de montagne des Pyrénées, ou montagne des Pyrénées est une race ancienne de chien de berger, utilisé dans le sud-ouest de la France et le nord-est de l\'Espagne, en particulier les Pyrénées pour la protection des troupeaux contre les prédateurs, notamment les ours qui y vivent.'
    );

INSERT INTO
    circuit (name, img, description)
VALUES (
        'Les petits pédestres',
        'https://images.pexels.com/photos/554609/pexels-photo-554609.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ), (
        'En route vers l\aventure',
        'https://images.pexels.com/photos/589841/pexels-photo-589841.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ), (
        'Au sommet nous iront',
        'https://images.pexels.com/photos/3680119/pexels-photo-3680119.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
        'Cette balade vous emmenera au sommet des montagnes avec une vue sur le domaine d\Alpadou'
    );

INSERT INTO
    user (
        name,
        lastname,
        phone,
        email,
        password,
        role
    )
VALUES (
        'Joseph',
        'Joestar',
        '',
        'jojo@mail.com',
        '$2y$13$M9/vQ2Qcxb1kfQh6kgMixOYSiFCwKOZ/BVYUPNy3b0D9.eOqVhbri',
        'ROLE_USER'
    )
    , (
        'Alpadou',
        '',
        '0633464450',
        'alpadou@mail.com',
        '$2y$13$n7zHwhN7VQboYBOgAUukNuH4Myu1kgxHDq0Pq2JRQADnVUSn43W4O',
        'ROLE_ADMIN'
    );

INSERT INTO animal_circuit (id_animal, id_circuit)
VALUES (1,1), (1,2), (2,1), (2,2), (3,1), (3,3);

INSERT INTO
    reservation (
        date,
        id_animal,
        id_circuit,
        id_user
    )
VALUES ('2023-07-26', 1, 1, 1), ('2023-09-04', 3, 3, 1);