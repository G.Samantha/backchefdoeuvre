<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Repository\AnimalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;


#[Route('/api/animal')]
class AnimalController extends AbstractController
{
    public function __construct(private AnimalRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        $animal = $this->repo->findAll();
        return $this->json($animal);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $animal = $this->repo->findById($id);
        if (!$animal) {
            throw new NotFoundHttpException();
        }
        return $this->json($animal);
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try {
            $animal = $serializer->deserialize($request->getContent(), Animal::class, 'json');
            $this->repo->persist($animal);

            return $this->json($animal, JsonResponse::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', JsonResponse::HTTP_BAD_REQUEST);
        }
    }


    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator):JsonResponse
    {
        $animal = $this->repo->findById($id);
        if (!$animal) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Animal::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id):JsonResponse
    {
        $animal = $this->repo->findById($id);
        if (!$animal) {
            throw new NotFoundHttpException();
        }
        $this->repo->delete($animal);
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

}