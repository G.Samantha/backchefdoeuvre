<?php

namespace App\Controller;

use App\Entity\Circuit;
use App\Repository\CircuitRepository;
use App\Repository\AnimalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;


#[Route('/api/circuit')]
class CircuitController extends AbstractController
{
    public function __construct(private CircuitRepository $circuitRepository, private AnimalRepository $animalRepository)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->circuitRepository->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $circuit = $this->circuitRepository->findById($id);
        if (!$circuit) {
            throw new NotFoundHttpException();
        }
        return $this->json($circuit);
    }
    
    #[Route('/{circuitId}/animals', methods: 'GET')]
    public function getCircuitAnimals(int $circuitId)
    {
        return $this->json($this->animalRepository->findByCircuitId($circuitId));
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try {
            $circuit = $serializer->deserialize($request->getContent(), Circuit::class, 'json');
            $this->circuitRepository->persist($circuit);

            return $this->json($circuit, JsonResponse::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', JsonResponse::HTTP_BAD_REQUEST);
        }
    }


    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator):JsonResponse
    {
        $circuit = $this->circuitRepository->findById($id);
        if (!$circuit) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Circuit::class, 'json');
            $toUpdate->setId($id);
            $this->circuitRepository->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id):JsonResponse
    {
        $circuit = $this->circuitRepository->findById($id);
        if (!$circuit) {
            throw new NotFoundHttpException();
        }
        $this->circuitRepository->delete($circuit);
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

}