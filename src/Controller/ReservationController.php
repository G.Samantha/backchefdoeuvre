<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Repository\AnimalRepository;
use App\Repository\CircuitRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;


#[Route('/api/reservation')]
class ReservationController extends AbstractController
{
    public function __construct(private ReservationRepository $repo)
    {
        $this->repo =$repo;
    }

    #[Route(methods: 'GET')]
    public function all()
    {
        return $this->json($this->repo->findAll());
    }

    // #[Route(methods: 'GET')]
    // public function all()
    // {
    //     $reservation =$this->repo->findAll();
    //     return $this->json($reservation);
    // }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $reservation = $this->repo->findById($id);
        if (!$reservation) {
            throw new NotFoundHttpException();
        }
        return $this->json($reservation);
    }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try {
            $content = json_decode($request->getContent());
            
            $date = new DateTime($content->date);

            $animalRepo = new AnimalRepository();
            $animal = $animalRepo->findById($content->animal);

            $circuitRepo = new CircuitRepository();
            $circuit = $circuitRepo->findById($content->circuit);

            $userRepo = new UserRepository();
            $user = $userRepo->findById($content->user);

            $reservation = new Reservation($date, $animal, $circuit, $user);

            $this->repo->persist($reservation);

            return $this->json($reservation, JsonResponse::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', JsonResponse::HTTP_BAD_REQUEST);
        }
    }


    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator):JsonResponse
    {
        $reservation = $this->repo->findById($id);
        if (!$reservation) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Reservation::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), JsonResponse::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id):JsonResponse
    {
        $reservation = $this->repo->findById($id);
        if (!$reservation) {
            throw new NotFoundHttpException();
        }
        $this->repo->delete($reservation);
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

    #[Route('/user/{id}', methods:'GET')]
    public function userReservation(int $id){
        $reservation = $this->repo->findReservationsByUser($id);
        return $this->json($reservation);
    }

}