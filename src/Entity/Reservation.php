<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

class Reservation {
private ?int $id;
private ?DateTime $date;
private ?Animal $animal;
private  ?Circuit $circuit;
private ?User $user;  

  /**
     * @param int|null $id
     * @param DateTime|null $date
     * @param int|null $animal
     * @param int|null $circuit
     * @param int|null $user
     */
    public function __construct(?DateTime $date, ?Animal $animal, ?Circuit $circuit,?User $user, ?int $id=null) {
    	$this->id = $id;
    	$this->date = $date;
    	$this->animal = $animal;
    	$this->circuit = $circuit;
    	$this->user = $user;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getAnimal(): ?Animal {
		return $this->animal;
	}
	
	/**
	 * @param  $animal 
	 * @return self
	 */
	public function setAnimal(?Animal $animal): self {
		$this->animal = $animal;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCircuit(): Circuit {
		return $this->circuit;
	}
	
	/**
	 * @param  $circuit 
	 * @return self
	 */
	public function setCircuit(Circuit $circuit): self {
		$this->circuit = $circuit;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getUser(): User {
		return $this->user;
	}
	
	/**
	 * @param  $user 
	 * @return self
	 */
	public function setUser(User $user): self {
		$this->user = $user;
		return $this;
	}
}
