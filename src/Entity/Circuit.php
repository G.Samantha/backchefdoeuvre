<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Circuit {
private ?int $id;
#[Assert\NotBlank]
private ?string $name;
private ?string $img;
private ?string $description; 


/**
     * @param int|null $id
     * @param string|null $name
     * @param string|null $img
     * @param string|null $description
     */
public function __construct(?string $name, ?string $img, ?string $description,?int $id=null){
$this->id=$id;
$this ->name=$name;
$this->img=$img;
$this->description=$description;
}


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param  $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param  $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}
}