<?php

namespace App\Repository;

use App\Entity\Circuit;
use PDO;

class CircuitRepository
{

    private PDO $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }
    private function sqlToCircuit(array $line): Circuit
    {
        return new Circuit($line['name'], $line['img'], $line['description'], $line['id']);
    }

    /**
     * Affiche tous les circuits
     * @return array
     */
    public function findAll(): array
    {
        $list = [];
        $query = $this->connection->prepare('SELECT * FROM circuit');
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $value) {
            $list[] = $this->sqlToCircuit($value);
        }
        return $list;
    }

    /**
     * Affiche le circuit correspondant à l'id selectionné
     * @param int $id
     * @return Circuit|null
     */
    public function findById(int $id): Circuit|null
    {
        $query = $this->connection->prepare('SELECT * FROM circuit WHERE id = :id');
        $query->bindValue('id', $id);
        $query->execute();
        $results = $query->fetch();
        if ($results) {
            return $this->sqlToCircuit($results);
        }

        return null;
    }

    /**
     * Ajout circuit à la base de donnée
     * @param Circuit $circuit
     * @return void
     */
    public function persist(Circuit $circuit)
    {

        $query = $this->connection->prepare('INSERT INTO circuit (name, img, description) VALUES (:name, :img, :description)');
        $query->bindValue('name', $circuit->getName());
        $query->bindValue('img', $circuit->getImg());
        $query->bindValue('description', $circuit->getDescription());

        $query->execute();

        $circuit->setId($this->connection->lastInsertId());
    }

    /**
     * Permet d'update le circuit via l'id
     * @param int $id
     * @param string $name
     * @param string $img
     * @param string $description
     * @return void
     */
    public function update(Circuit $circuit)
    {
        $query = $this->connection->prepare('UPDATE circuit SET name=:name, img=:img, description=:description WHERE id=:id');

        $query->bindValue(':name', $circuit->getName());
        $query->bindValue(':img', $circuit->getImg());
        $query->bindValue(':descirption', $circuit->getDescription());
        $query->bindValue(':id', $circuit->getId());

        $query->execute();
    }

    /**
     * Delete le circuit par l'id
     * @param Circuit $circuit
     * @return void
     */
    public function delete(Circuit $circuit)
    {
        $query = $this->connection->prepare('DELETE FROM circuit WHERE id=:id');
        $query->bindValue('id', $circuit->getId());

        $query->execute();
    }
}