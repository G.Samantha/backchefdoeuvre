<?php
namespace App\Repository;

use App\Entity\User;
use PDO;

class UserRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }

    private function sqlToUser(array $line): User
    {
        return new User($line['name'], $line['lastname'], $line['phone'],
         $line['email'], $line['password'], $line['role'], $line['id']);
    }

    public function findAll(): array
    {
        $list = [];
        $query = $this->connection->prepare('SELECT * FROM user');
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $value) {
            $list[] = $this->sqlToUser($value);
        }
        return $list;
    }

  /**
     * Affiche le user correspondant à l'id selectionné
     * @param int $id
     * @return User|null
     */
    public function findById(int $id): User|null
    {
        $query = $this->connection->prepare('SELECT * FROM user WHERE id = :id');
        $query->bindValue('id', $id);
        $query->execute();
        $results = $query->fetch();
        if ($results) {
            return $this->sqlToUser($results);
        }
        return null;
    }

    /**
     * Ajout un user à la base de donnée
     * @param User $user
     * @return void
     */
    public function persist(User $user) {

        $query = $this->connection->prepare('INSERT INTO user (name, lastname, phone, email, password, role) VALUES (:name, :lastname, :phone, :email,:password,:role)');
        $query->bindValue('name', $user->getName());
        $query->bindValue('lastname', $user->getLastName());
        $query->bindValue('phone', $user->getPhone());
        $query->bindValue('email', $user->getEmail());
        $query->bindValue('password', $user->getPassword());
        $query->bindValue('role', $user->getRole());
    
        $query->execute();
        
        $user->setId($this->connection->lastInsertId());
    }

 /**
     * Permet d'update le user via l'id
     * @param int $id
     * @param string $name
     * @param string $lastname
     * @param string $phone
     * @param string $email
     * @param string $password
     * @param string $role
     * @return void
     */
    public function update (User $user){
        $query = $this->connection->prepare('UPDATE user SET name=:name, lastname=:lastname, phone=:phone, email=:email, password=:password, role=:role WHERE id=:id');

        $query->bindValue(':name', $user->getName());
        $query->bindValue(':lastname', $user->getLastName());
        $query->bindValue(':phone', $user->getPhone());
        $query->bindValue(':email', $user->getEmail());
        $query->bindValue(':password', $user->getPassword());
        $query->bindValue(':role', $user->getRole());
        $query->bindValue(':id', $user->getId());

        $query->execute();
    }

 /**
     * Delete le user par l'id
     * @param User $user
     * @return void
     */
    public function delete (User $user){
        $query = $this->connection->prepare('DELETE FROM user WHERE id=:id');
        $query->bindValue('id', $user->getId());
        $query->execute();
    }

    public function findByEmail(string $email): ?User
    {

        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT * FROM user WHERE email=:email');
        $query->bindValue(':email', $email);
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            return new User($line['name'], $line['lastname'], $line['phone'], $line['email'], $line['password'], $line['role'], $line['id']);
        }
        return null;
    }
}