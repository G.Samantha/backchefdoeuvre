<?php

namespace App\Repository;
use App\Entity\Animal;
use PDO;

class AnimalRepository {

    private PDO $connection;
    public function __construct(){
        $this->connection = Connection::getConnection();
    }
    private function sqlToAnimal(array $line):Animal {
        return new Animal($line['name'], $line['img'], $line['description'], $line['id']);
    }
    /**
     * Affiche tous les animaux
     * @return array
     */
    public function findAll():array {
        $query = $this->connection->prepare('SELECT * FROM animal');
        $query ->execute();
        $results = $query->fetchAll();

        foreach ($results as $value){
            $animals[] =$this->sqlToAnimal($value);
        }
        return $animals;
    }
    /**
     * Affiche l'animal correspondant à l'id selectionné
     * @param int $id
     * @return Animal|null
     */
    public function findById(int $id): Animal|null
    {
        $query = $this->connection->prepare('SELECT * FROM animal WHERE id = :id');
        $query->bindValue('id', $id);
        $query->execute();
        $results = $query->fetch();
        if ($results) {
            return $this->sqlToAnimal($results);
        }

        return null;
    }

   /**
     * Récupère les animaux qui sont liés à un ID d'un circuit
     * @param int $circuitId
     * @return array|null
     */
    public function findByCircuitId(int $circuitId): array|null
    {
        $query = $this->connection->prepare('SELECT animal.* FROM animal_circuit ac INNER JOIN animal ON ac.id_animal = animal.id WHERE ac.id_circuit = :id');
        $query->bindValue('id', $circuitId);
        $query->execute();
        $results = $query->fetchAll();
        if ($results) {
            foreach ($results as $value){
                $animals[] =$this->sqlToAnimal($value);
            }
            return $animals;
        }

        return null;
    }


 /**
     * Ajout animal à la base de donnée
     * @param Animal $animal
     * @return void
     */
    public function persist(Animal $animal) {

        $query = $this->connection->prepare('INSERT INTO animal (name, img, description) VALUES (:name, :img, :description)');
        $query->bindValue('name', $animal->getName());
        $query->bindValue('img', $animal->getImg());
        $query->bindValue('description', $animal->getDescription());

        $query->execute();
        
        $animal->setId($this->connection->lastInsertId());
    }

    /**
     * Permet d'update l'animal via l'id
     * @param int $id
     * @param string $name
     * @param string $img
     * @param string $description
     * @return void
     */
    public function update (Animal $animal){
        $query = $this->connection->prepare('UPDATE animal SET name=:name, img=:img,description=:description WHERE id=:id');

        $query->bindValue(':name', $animal->getName());
        $query->bindValue(':img', $animal->getImg());
        $query->bindValue(':description', $animal->getDescription());
        $query->bindValue(':id', $animal->getId());

        $query->execute();
    }

    /**
     * Delete l'animal par l'id
     * @param Animal $animal
     * @return void
     */
    public function delete (Animal $animal){
        $query = $this->connection->prepare('DELETE FROM animal WHERE id=:id');
        $query->bindValue('id', $animal->getId());

        $query->execute();
    }
}