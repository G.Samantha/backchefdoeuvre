<?php

namespace App\Repository;

use App\Entity\Reservation;
use App\Repository\AnimalRepository;
use App\Repository\CircuitRepository;
use App\Repository\UserRepository;
use PDO;
use DateTime;

class ReservationRepository
{

    private PDO $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }
    private function sqlToReservation(array $line): Reservation
    {
        $date = null;
        if (isset($line['date'])) {
            $date = new DateTime($line['date']);
        }

        $animalRepo = new AnimalRepository();
        $animal = $animalRepo->findById($line['id_animal']);

        $circuitRepo = new CircuitRepository();
        $circuit = $circuitRepo->findById($line['id_circuit']);

        $userRepo = new UserRepository();
        $user = $userRepo->findById($line['id_user']);


        return new Reservation($date, $animal, $circuit, $user, $line['id']);
    }

    /**
     * Affiche toutes les reservations
     * @return array
     */
    public function findAll(): array
    {

        $list = [];
        $query = $this->connection->prepare('SELECT * FROM reservation');
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $value) {
            $list[] = $this->sqlToReservation($value);
        }

        return $list;
    }

    /**
     * Affiche la reservation correspondant à l'id selectionné
     * @param int $id
     * @return Reservation|null
     */
    public function findById(int $id): Reservation|null
    {
        $query = $this->connection->prepare('SELECT * FROM reservation WHERE id = :id');
        $query->bindValue('id', $id);
        $query->execute();
        $results = $query->fetch();
        $date = null;
        if (isset($results['date'])) {
            $date = new DateTime($results['date']);
        }
        if ($results) {
            return $this->sqlToReservation($results);
        }

        return null;
    }

    /**
     * 
     * @param Reservation $reservation
     * @return void
     */
    public function persist(Reservation $reservation)
    {

        $query = $this->connection->prepare('INSERT INTO reservation (date, id_animal, id_circuit, id_user) VALUES (:date, :animal, :circuit, :user)');
        $query->bindValue('date', $reservation->getDate()->format('Y-m-d'));
        $query->bindValue('animal', $reservation->getAnimal()->getId());
        $query->bindValue('circuit', $reservation->getCircuit()->getId());
        $query->bindValue('user', $reservation->getUser()->getId());

        $query->execute();

        $reservation->setId($this->connection->lastInsertId());
    }

    /**
     * Permet d'update la reservation via l'id
     * @param int $id
     * @param DateTime $date
     * @param string $animal
     * @param string $circuit
     * @param string $user
     * @return void
     */
    public function update(Reservation $reservation)
    {
        $query = $this->connection->prepare('UPDATE reservation SET date=:date, animal=:animal, circuit=:circuit, user=:user WHERE id=:id');

        $query->bindValue(':date', $reservation->getDate()->format('Y-m-d'));
        $query->bindValue(':animal', $reservation->getAnimal());
        $query->bindValue(':circuit', $reservation->getCircuit());
        $query->bindValue(':user', $reservation->getUser());
        $query->bindValue(':id', $reservation->getId());

        $query->execute();
    }

    /**
     * Delete la reservation par l'ID
     * @param Reservation $reservation
     * @return void
     */
    public function delete(Reservation $reservation)
    {
        $query = $this->connection->prepare('DELETE FROM reservation WHERE id=:id');
        $query->bindValue('id', $reservation->getId());

        $query->execute();
    }

    public function findReservationsByUser(int $id): array
    {
        $list = [];

        $query = $this->connection->prepare("SELECT * FROM reservation WHERE id_user = :id_user");
        $query->bindValue('id_user', $id);
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $line) {
            $animalRepo = new AnimalRepository();
            $animal = $animalRepo->findById($line['id_animal']);

            $circuitRepo = new CircuitRepository();
            $circuit = $circuitRepo->findById($line['id_circuit']);

            $userRepo = new UserRepository();
            $user = $userRepo->findById($line['id_user']);

            $date = new DateTime($line['date']);
            $list[] = new Reservation($date, $animal, $circuit, $user, $line['id']);
        }

        return $list;
    }

}